syntax on
colorscheme ron

set ai 
set nocompatible
set cindent
set number
set relativenumber
set autoindent
set smartindent
set ignorecase 
set tabstop=4
set shiftwidth=4
set formatoptions-=cro
set expandtab

nnoremap <F10> :Explore<CR>
nnoremap <F9> :make<CR>
nnoremap <F7> :tabprevious<CR>
nnoremap <F8> :tabnex<CR>
