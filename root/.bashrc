shopt -s histappend
shopt -s checkwinsize
shopt -s autocd
shopt -s cdspell

PS1="\[\033[31;5;15m\]\u\[$(tput sgr0)\]\[\033[38;5;15m\]@\h:\[$(tput sgr0)\]\[\033[38;5;6m\][\w]:\[$(tput sgr0)\]\[\033[38;5;15m\] \[$(tput sgr0)\]"

if [ -f /etc/bash_completion]; then 
	. /etc/bash_completion
fi

export EDITOR=vim
export VISUAL=vim

alias vi='vim'
alias shutdown='sudo shutdown now'
alias reboot='sudo shutdown -r now'
alias ls='ls --color=auto'

