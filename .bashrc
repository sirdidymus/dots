shopt -s histappend
shopt -s checkwinsize
shopt -s autocd
shopt -s cdspell

PS1="\[\033[38;5;11m\]\u\[$(tput sgr0)\]\[\033[38;5;15m\]@\h:\[$(tput sgr0)\]\[\033[38;5;6m\][\w]:\[$(tput sgr0)\]\[\033[38;5;15m\] \[$(tput sgr0)\]"

export EDITOR=vim
export VISUAL=vim
export GOROOT=/usr/lib/golang
export GOPATH=$HOME/go
export PATH=$GOPATH/bin:$GOROOT/bin:$PATH
export WLC_REPEAT_RATE=50
export WLC_REPEAT_DELAY=250

xset r rate 250 50
setxkbmap -option ctrl:nocaps

alias ls='ls --color=auto'
alias vi='vim'
alias shutdown='sudo shutdown now'
alias reboot='sudo shutdown -r now'
alias online="w -h | cut -f1 -d \" \" | uniq"
alias diffs='diff --side-by-side --suppress-common-lines'


if [ -f /etc/bash_completion ]; then
        . /etc/bash_completion
fi
